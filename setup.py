from setuptools import setup, find_packages
from sys import version_info as py_version

VERSION_REQUIREMENTS = []
MODULES = []

if py_version < (3, 5):
    MODULES.append('subprocess35')
    VERSION_REQUIREMENTS.append('typing')

setup(
    name='pyi3',
    packages=find_packages(),
    install_requires=VERSION_REQUIREMENTS,
    py_modules=MODULES,
)
