pyi3
====

A type-hinted python module to communicate with i3.

Somewhat based on [i3ipc-python](https://github.com/acrisci/i3ipc-python),
but no code is shared.

Better documentation coming soon. Clean under pylint, mypy and flake8.

Tests to be added.
